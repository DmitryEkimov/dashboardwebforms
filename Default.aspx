﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DashboardWebForms._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

<!-- top table 1 (Opens and clicks)-->
<table class="table-style" id="top-table1" bordercolor = "orange">
    <tr>
        <th colspan="4"><span class="thick">Opens & Clicks</span></th>
    </tr>
    <tr>
        <td class="thick">Sent</td>
        <td>76</td>
        <td>100%</td>
        <td>
            <img src="images/glass.png" class="glass" onclick="show()">
            <img src="images/excel.png" class="excel" onclick="show()">
        </td>
    </tr>
    <tr>
        <td class="thick">Unique Opens / Open Rate<img src="images/question.png" class="question"></td>
        <td class="thick">64</td>
        <td class="thick">84%</td>
        <td>
            <img src="images/glass.png" class="glass" onclick="show()">
            <img src="images/excel.png" class="excel" onclick="show()">
        </td>
    </tr>
    <tr>
        <td class="thick">Total Opens<img  src="images/question.png" class="question"></td>
        <td>69</td>
        <td></td>
        <td></td>
    </tr>
    <tr class="grey-background">
        <td class="thick">Not opened<img src="images/question.png" class="question"></td>
        <td>12</td>
        <td>16%</td>
        <td>
            <img src="images/glass.png" class="glass" onclick="show()">
            <img src="images/excel.png" class="excel" onclick="show()">
        </td>
    </tr>
    <tr>
        <td class="thick">Unique Clicks / Click Rate<img src="images/question.png" class="question"></td>
        <td class="thick">18</td>
        <td class="thick">28%</td>
        <td>
            <img src="images/glass.png" class="glass" onclick="show()">
            <img src="images/excel.png" class="excel" onclick="show()">
        </td>
    </tr>
    <tr>
        <td class="thick">Total Clicks<img src="images/question.png" class="question"></td>
        <td>52</td>
        <td></td>
        <td></td>
    </tr>
</table>

<!-- top table 2 (Reader Averages)-->

<table class="table-style" id="top-table2">
    <tr>
        <th colspan="4" class="thick">Reader Averages</th>
    </tr>
    <tr>
        <td class="thick">Opens<img src="images/question.png" class="question"></td>
        <td class="thick">2.2 times</td>
    </tr>
    <tr>
        <td class="thick">Clicks<img src="images/question.png" class="question"></td>
        <td class="thick">2.5 clicks</td>
    </tr>
    <tr>
        <td class="thick">1st Click<img  src="images/question.png" class="question"></td>
        <td class="thick">0:15</td>
    </tr>
    <tr>
        <td class="thick">Time Spent Clicking<img src="images/question.png" class="question"></td>
        <td class="thick">5:14</td>
    </tr>
    <tr>
        <td class="thick">Time Spent on a Landing Page<img src="images/question.png" class="question"></td>
        <td class="thick">0:15</td>
    </tr>
    <tr>
        <td class="thick">Scroll Depth<img src="images/question.png" class="question"></td>
        <td class="thick">35%</td>
    </tr>
</table>

<!-- top table 3 (Content)-->

<table class="table-style" id="top-table3">
    <tr>
        <th class="thick" colspan="4" >Content</th>
    </tr>
    <tr>
        <td class="thick">Word Count</td>
        <td class="thick">1506</td>
    </tr>
    <tr>
        <td class="thick">Subject Line Character Count</td>
        <td class="thick">78</td>
    </tr>
    <tr>
        <td class="thick"># Articles</td>
        <td class="thick">10</td>
    </tr>
    <tr>
        <td class="thick"># Images</td>
        <td class="thick">8</td>
    </tr>
    <tr>
        <td class="thick"># Links</td>
        <td class="thick">15</td>
    </tr>
    <tr>
        <td class="thick"># Videos</td>
        <td class="thick">0</td>
    </tr>
</table>


  <p class="headline"><select name="sort">
    <option value="value_total">Percentage based on Value Total</option>
    <option value="clicks_total">Percentage based on Clicks Total</option>
  </select>
  <img src="images/question.png" class="question"></p>
  
        <asp:Repeater ID="rptCustomers" runat="server" OnItemDataBound="rptCustomers_ItemDataBound">
        <HeaderTemplate>
			<table class="table-style" id="big-center-table" class="big-center-table">
				<tr class="grey-background">
					<td class="width" >Clicks Activity</td>
					<td class="thick" style="width:200px">Unique Clicks</td>
					<td class="thick" style="width:200px">Total Clicks</td>
					<td class="questioncentertabeleft" style="width:200px">Page Hits<img src="images/question.png" class="question" class="img-width"></td>
					<td class="thick" style="width:16px">Likes</td>
					<td class="thick" style="width:100px">Comments</td>
					<td class="thick" style="width:100px">Rating</td>
					<td class="thick" style="width:100px">Visit time<img src="images/question.png" class="question" class="img-width2"></td>
				</tr>
        </HeaderTemplate>
        <ItemTemplate>
		     <tr>
				<td class="width">
					<p class="blue-font"><asp:Label ID="Label1" runat="server" Text='<%# Eval("ClicksActivity") %>' /></p>
					<p>Read & Comment > Landing Page (auto)</p>
                    <%# (int)Eval("Index") == 5 ? "<p><span class=\"blue-font\" align=\"right\">More Links</span><img src=\"images/more.png\" class=\"morelinks\"></p>": "" %>
				</td>
				<td><asp:Label ID="Label2" runat="server" Text='<%# Eval("UniqueClicks") %>' /><span class="italic"> (<asp:Label ID="Label22" runat="server" Text='<%# Eval("UniqueClicksPercent") %>' />%)</span><img src="images/glass.png" class="glass" onclick="show()"></td>
				<td><asp:Label ID="lblCustomerId" runat="server" Text='<%# Eval("TotalClicks") %>' /><span class="italic"> (<asp:Label ID="Label77" runat="server" Text='<%# Eval("TotalClicksPercent") %>' />%)</span><img src="images/glass.png" class="glass" onclick="show()"></td>
				<td class="img-width"><asp:Label ID="Label3" runat="server" Text='<%# Eval("PageHits") %>' /></td>
				<td><asp:Label ID="lblContactName" runat="server" Text='<%# Eval("Likes") %>' /><span class="italic"> (<asp:Label ID="Label27" runat="server" Text='<%# Eval("LikesPercent") %>' />%)</span> <img src="images/glass.png" class="glass" onclick="show()"></td>
				<td><asp:Label ID="Label5" runat="server" Text='<%# Eval("Comments") %>' /><span class="italic"> (<asp:Label ID="Label17" runat="server" Text='<%# Eval("CommentsPercent") %>' />%)</span> <img src="images/glass.png" class="glass" onclick="show()"></td>
				<td><asp:Label ID="Label4" runat="server" Text='<%# Eval("Rating") %>' /></td>
				<td class="img-width2"><asp:Label ID="Label6" runat="server" Text='<%# Eval("VisitTime") %>' /></td>
			</tr>
        </ItemTemplate>
        <FooterTemplate>
            <tr class="grey-background" style="height:35px;" >
				<td class="width">
					<p>Total</p>
				</td>
				<td class="thick" ><asp:Label ID="totalUniqueClicks" runat="server"   /></td>
				<td class="thick" ><asp:Label ID="TotalClicks" runat="server"  /></td>
				<td class="thick img-width" ><asp:Label ID="totalPageHits" runat="server"  /></td>
				<td class="thick" ><asp:Label ID="totalLikes" runat="server"  /></td>
				<td class="thick" ><asp:Label ID="totalComments" runat="server"  /></td>
				<td></td>
				<td class="img-width2"></td>
			</tr>
		</table>
        </FooterTemplate>
    </asp:Repeater>

<!-- bottom table 5 (Clicks Activity)-->

<table class="table-style" id="bottom-table5">
    <tr>
        <th class="thick" colspan="4">Useful Clicks</th>
    </tr>
    <tr>
        <td class="thick">Forwarded</td>
        <td>2</td>
        <td>
            <img src="images/glass.png" class="glass" onclick="show()">
            <img src="images/excel.png" class="excel" onclick="show()">
        </td>
    </tr>
    <tr>
        <td class="thick">Replied</td>
        <td>0</td>
        <td>
            <img src="images/glass.png" class="glass" onclick="show()">
            <img src="images/excel.png" class="excel" onclick="show()">
        </td>
    </tr>
    <tr>
        <td class="thick">Printed</td>
        <td>3</td>
        <td>
            <img src="images/glass.png" class="glass" onclick="show()">
            <img src="images/excel.png" class="excel" onclick="show()">
        </td>
    </tr>
    <tr>
        <td class="thick">Browser View</td>
        <td>2</td>
        <td>
            <img src="images/glass.png" class="glass" onclick="show()">
            <img src="images/excel.png" class="excel" onclick="show()">
        </td>
    </tr>
    <tr>
        <td class="thick">Previos Editions</td>
        <td>2</td>
        <td>
            <img src="images/glass.png" class="glass" onclick="show()">
            <img src="images/excel.png" class="excel" onclick="show()">
        </td>
    </tr>
    <tr>
        <td class="thick">Subscribed</td>
        <td>1</td>
        <td>
            <img src="images/glass.png" class="glass" onclick="show()">
            <img src="images/excel.png" class="excel" onclick="show()">
        </td>
    </tr>
    <tr class="grey-background">
        <td class="thick">Unsubscribed</td>
        <td>0</td>
        <td>
            <img src="images/glass.png" class="glass" onclick="show()">
            <img src="images/excel.png" class="excel" onclick="show()">
        </td>
    </tr>
</table>

<!-- bottom table 6 (Clicks Activity)-->

<table class="table-style" id="bottom-table6">
    <tr>
        <th  class="thick" colspan="4">Delivery information</th>
    </tr>
    <tr>
        <td class="thick">Attempted</td>
        <td>76</td>
        <td>     </td>
    </tr>
    <tr class="grey-background">
        <td class="thick">Bounce-Backs <img src="images/question.png" class="question"></td>
        <td>0</td>
        <td>
            <img src="images/glass.png" class="glass" onclick="show()">
            <img src="images/excel.png" class="excel" onclick="show()">
        </td>
    </tr>
    <tr class="grey-background">
        <td class="thick">Undelivered <img src="images/question.png" class="question"></td>
        <td>0</td>
        <td>
            <img src="images/glass.png" class="glass" onclick="show()">
            <img src="images/excel.png" class="excel" onclick="show()">
        </td>
    </tr>
</table>

<!-- bottom table 7 (Clicks Activity)-->

<table class="table-style" id="bottom-table7">
    <tr>
        <th class="thick"colspan="4">Campaign Info</th>
    </tr>
    <tr>
        <td class="thick">Owner</td>
        <td>Jessica</td>
    </tr>
    <tr>
        <td class="thick">Sent Date</td>
        <td>Monday 06/23/18 06:44 PM</td>
    </tr>
    <tr>
        <td class="thick">From name</td>
        <td>Corporate Communications</td>
    </tr>
    <tr class="grey-background">
        <td class="thick">Subject</td>
        <td>Inspired News/July 2018</td>
    </tr>
    <tr>
        <td class="thick">Recipients</td>
        <td><select name="ALL EMPLOYEYS">
		<option value="value_ALL EMPLOYEYS">ALL EMPLOYEYS</option>
        </select></td>
    </tr>
    <tr>
        <td class="thick">Total count <img src="images/question.png" class="question"></td>
        <td>76</td>
    </tr>

</table>

<!-- Modal window-->
<div id="myModal" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <a href="#close" title="Закрыть" class="close">X</a>
            <div class="blue">Click on X for close form</div>
        </div>
    </div>
</div>

</asp:Content>
