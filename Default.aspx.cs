﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DashboardWebForms
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.BindRepeater();
            }
        }

        private void BindRepeater()
        {
           this.rptCustomers.DataSource = source;
            this.rptCustomers.DataBind();
        }
        public Item[] source = new Item[5]{
                new Item(){ ClicksActivity="Organization announcement from the CEO", UniqueClicks=16, TotalClicks=19, PageHits=19, Likes=8,Comments=3, Rating=0, VisitTime="00:40", Index=1,UniqueClicksPercent=33,TotalClicksPercent=37,LikesPercent=47, CommentsPercent=100 },
                new Item(){ ClicksActivity="AGC honored with a Super Empresas Award", UniqueClicks=8, TotalClicks=8, PageHits=8, Likes=8,Comments=0, Rating=0, VisitTime="01:44", Index=2,UniqueClicksPercent=17,TotalClicksPercent=15,LikesPercent=0, CommentsPercent=0 },
                new Item(){ ClicksActivity="HRsys system outage this weekend", UniqueClicks=6, TotalClicks=6, PageHits=6, Likes=8,Comments=0, Rating=0, VisitTime="00:04", Index=3,UniqueClicksPercent=12,TotalClicksPercent=12,LikesPercent=0, CommentsPercent=0  },
                new Item(){ ClicksActivity="AGC raise $30,000 to end woman's cancers", UniqueClicks=5, TotalClicks=5, PageHits=19, Likes=8,Comments=0, Rating=0, VisitTime="00:04",  Index=4,UniqueClicksPercent=10,TotalClicksPercent=10,LikesPercent=0, CommentsPercent=0 },
                new Item(){ ClicksActivity="Vancouver in Pictures", UniqueClicks=5, TotalClicks=5, PageHits=5, Likes=8,Comments=0, Rating=0, VisitTime="01:43", Index=5,UniqueClicksPercent=10,TotalClicksPercent=10,LikesPercent=0, CommentsPercent=0  }
        };
        protected void rptCustomers_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Footer) return;

            Label lbl1 = (Label)e.Item.FindControl("totalUniqueClicks");
            lbl1.Text = source.Sum(s=>s.UniqueClicks).ToString();

            Label lbl2 = (Label)e.Item.FindControl("TotalClicks");
            lbl2.Text = source.Sum(s => s.TotalClicks).ToString();

            Label lbl3 = (Label)e.Item.FindControl("totalPageHits");
            lbl3.Text = source.Sum(s => s.PageHits).ToString();

            Label lbl4 = (Label)e.Item.FindControl("totalLikes");
            lbl4.Text = source.Sum(s => s.Likes).ToString();

            Label lbl5 = (Label)e.Item.FindControl("totalComments");
            lbl5.Text = source.Sum(s => s.Comments).ToString();
        }
    }

    public class Item
    {
        public string ClicksActivity { get; set; }
        public int UniqueClicks { get; set; }
        public int TotalClicks { get; set; }
        public int PageHits { get; set; }
        public int Likes { get; set; }
        public int Comments { get; set; }
        public int Rating { get; set; }
        public string VisitTime { get; set; }
        public int Index { get; set; }
        public int UniqueClicksPercent { get; set; }
        public int TotalClicksPercent { get; set; }
        public int LikesPercent { get; set; }
        public int CommentsPercent { get; set; }
    }


}